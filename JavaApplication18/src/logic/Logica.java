/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import domain.ManejoArchivos;
import domain.Persona;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Logica {

    ArrayList<Persona> person = new ArrayList<>();
    private  ManejoArchivos ma;
    private String RUTA_USER = "Data/user.txt";

    public Logica() {
        ma = new ManejoArchivos();
    }
    
    
    
    
    public void print(){
        String txt = null;
        for (Persona persona : person) {
            txt += "Nombre: " + persona.getName() + "\n";
        }
        JOptionPane.showMessageDialog(null, txt);
    }

    public String recargarDatos() {
        String datos = "";
        FileReader fr;
        BufferedReader br = null ;
        try {
            fr = new FileReader("Data/data.txt");
            br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                datos += linea + "\n";
            }
        } catch (Exception e) {
            System.out.println("Error E/S: " + e);

        }finally{
            try {
                if (null != br) {
                    br.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }        
        return  datos;
    }
    
    
    
}
