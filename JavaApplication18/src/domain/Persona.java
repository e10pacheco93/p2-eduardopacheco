/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Persona {

    String user, name, lastname, password, typeUser;
    boolean activo;
    ArrayList<Persona> person = new ArrayList<>();

    public Persona() {
        activo = false;
    }

    public Persona(String user, String name, String lastname, String password, String typeUser) {
        this.user = user;
        this.name = name;
        this.lastname = lastname;
        this.password = password;
        this.typeUser = typeUser;
    }

    public String isActivo(Persona person) {
        String activo;
        if (person.activo) {
            activo = String.valueOf(true);
        } else {
            activo = String.valueOf(true);

        }
        return activo;
    }

    public void mostrar() {
        String matriz[][] = new String[person.size()][4];

        for (int i = 0; i < person.size(); i++) {
            matriz[i][0] = person.get(i).getName();
            matriz[i][1] = person.get(i).getLastname();
            matriz[i][2] = person.get(i).isActivo(person.get(i));
            matriz[i][3] = person.get(i).getTypeUser();

        }
    }

    //Metodos
    //registrar
    public void register(Persona persona) {
        String datos = "";
        person.add(persona);
        for (Persona persona1 : person) {
            datos += persona1.getUser() + " , " + persona1.getName() + " , " + persona1.getLastname() + " , " + persona1.getPassword() + " , " + persona1.getTypeUser();
        }

        try {
            File fichero = new File("Data/data.txt");
            if (!fichero.exists()) {
                fichero.createNewFile();
            }

            FileWriter fw = new FileWriter(fichero);
            fw.write(datos);
            fw.write("\r\n");

            fw.close();
            JOptionPane.showMessageDialog(null, "Persona registrada con exito");

        } catch (Exception e) {
            System.out.println("Error E/S: " + e);
        }
    }

    public void listpersons() {
        String data = "Lista de personas: \n";
        for (Persona persona : person) {
            for (int i = 0; i < person.size(); i++) {
                if (persona != null) {
                    data += i + "Nombre: " + persona.getName() + " - " + persona.typeUser + "\n";
                } else {
                    data += "______________________ \n";
                }
            }
        }
        JOptionPane.showMessageDialog(null, data);
    }

    public boolean existePerson(String user) {

        for (Persona persona : person) {
            if (persona != null && persona.getUser().contains(user)) {

                JOptionPane.showMessageDialog(null, "Persona ingresada...");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Usuario no existe, registrelo. ");

            }

        }

        return false;
    }

    public boolean existeAdmin(String user) {

        for (Persona persona : person) {
            if (persona != null && persona.getUser().contains(user) && persona.getTypeUser().contains("Administrador General")) {

                JOptionPane.showMessageDialog(null, "Persona ingresada...");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Usuario no existe, registrelo. ");

            }

        }

        return false;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    @Override
    public String toString() {
        return "persona{" + "user=" + user + ", name=" + name + ", lastname=" + lastname + ", password=" + password + ", typeUser=" + typeUser + '}';
    }

}
